from tensorflow.keras.models import load_model
from functions import load_image

if __name__ == "__main__":

    # load model
    model = load_model("cnn_classifier2.h5")

    # image path
    img_path = 'predictions/test.jpg'    # Path of the test image

    # load a single image
    new_image = load_image(img_path)

    # check prediction
    pred = model.predict(new_image)

    print(pred)
