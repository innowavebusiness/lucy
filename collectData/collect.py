# imports
import json
import functions

from google_images_download import google_images_download


# class instantiation
search = google_images_download.googleimagesdownload()

arguments = ""
with open("arguments.json", "r") as tempData:
    arguments = json.load(tempData)

print(arguments)

# passing the arguments to the function
paths = search.download(arguments)

# printing absolute paths of the downloaded images
#print(paths)

# Sanitizing
functions.sanitize_images('downloads', 'jpg')

# splitting dataset
functions.split_dataset_into_test_and_train_sets("downloads", "../train_data", "../test_data", 0.1)
