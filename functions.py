# functions

import os
import shutil
import glob
import numpy as np
import matplotlib.pyplot as plt

from PIL import Image
from keras.preprocessing import image


# rename files in downloads
def renameDownloads(folder_path, old, new):
    for path, subdirs, files in os.walk(folder_path):
        for name in files:
            if old.lower() in name.lower():
                file_path = os.path.join(path, name)
                new_name = os.path.join(path, name.lower().replace(old, new))
                os.rename(file_path, new_name)


# split downloads into training set and testing set and copy images in train_data and test_data dirs
def split_dataset_into_test_and_train_sets(all_data_dir, training_data_dir, testing_data_dir, testing_data_pct):
    # Recreate testing and training directories
    if testing_data_dir.count('/') > 1:
        shutil.rmtree(testing_data_dir, ignore_errors=False)
        os.makedirs(testing_data_dir)
        print("Successfully cleaned directory " + testing_data_dir)
    else:
        print(
            "Refusing to delete testing data directory " + testing_data_dir + " as we prevent you from doing stupid things!")

    if training_data_dir.count('/') > 1:
        shutil.rmtree(training_data_dir, ignore_errors=False)
        os.makedirs(training_data_dir)
        print("Successfully cleaned directory " + training_data_dir)
    else:
        print(
            "Refusing to delete testing data directory " + training_data_dir + " as we prevent you from doing stupid things!")

    num_training_files = 0
    num_testing_files = 0

    for subdir, dirs, files in os.walk(all_data_dir):
        category_name = os.path.basename(subdir)

        # Don't create a subdirectory for the root directory
        print(category_name + " vs " + os.path.basename(all_data_dir))
        if category_name == os.path.basename(all_data_dir):
            continue

        training_data_category_dir = training_data_dir + '/' + category_name
        testing_data_category_dir = testing_data_dir + '/' + category_name

        if not os.path.exists(training_data_category_dir):
            os.mkdir(training_data_category_dir)

        if not os.path.exists(testing_data_category_dir):
            os.mkdir(testing_data_category_dir)

        for file in files:
            input_file = os.path.join(subdir, file)
            if np.random.rand(1) < testing_data_pct:
                shutil.copy(input_file, testing_data_dir + '/' + category_name + '/' + file)
                num_testing_files += 1
            else:
                shutil.copy(input_file, training_data_dir + '/' + category_name + '/' + file)
                num_training_files += 1

    print("Processed " + str(num_training_files) + " training files.")
    print("Processed " + str(num_testing_files) + " testing files.")


def sanitize_images(path, file_extension):
    images = [img for img in glob.glob(path + "\\**\\*." + file_extension, recursive=True)]
    for img in images:
        try:
            image = Image.open(img)  # open the image file
            image.verify()  # verify that it is, in fact an image
        except (IOError, SyntaxError) as e:
            print('Bad file (will be removed):', img)  # print out the names of corrupt files
            os.remove(img)


# Load an image file to an array and returns the array after checking its validity
def load_image(img_path, show=False):
    img = image.load_img(img_path, target_size=(256, 256))
    img_tensor = image.img_to_array(img)  # (height, width, channels)
    img_tensor = np.expand_dims(img_tensor,
                                axis=0)  # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
    img_tensor /= 255.  # imshow expects values in the range [0, 1]

    if show:
        plt.imshow(img_tensor[0])
        plt.axis('off')
        plt.show()

    return img_tensor
