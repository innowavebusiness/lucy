from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Activation, Dropout, BatchNormalization

from keras.preprocessing.image import ImageDataGenerator

# Init model
model = Sequential()

# First Convolutional layer
model.add(Conv2D(32, (3, 3), input_shape=(256, 256, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

# Second Convolutional layer
model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

# Third Convoluntional layer
model.add(Conv2D(128, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

# Fourth Convoluntional layer
model.add(Conv2D(256, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

# Flattening
model.add(Flatten())

# Connection
model.add(Dense(2048))
model.add(BatchNormalization())
model.add(Activation('relu'))

model.add(Dense(49))    # 49 classes
model.add(BatchNormalization())
model.add(Activation('softmax'))


# Compiling the CNN
model.compile(optimizer='adamax', loss='categorical_crossentropy', metrics=['accuracy'])

# Training the CNN

# Split dataset using ImageDataGenerator validation_split
# (If used, you should disable the call to split_dataset_into_test_and_train_sets function) !!!

# data_generator = ImageDataGenerator(rescale=1./255, validation_split=0.10)
#
# train_datagen = data_generator.flow_from_directory(downloads,
#                                                      target_size=(128, 128),
#                                                      shuffle=True, seed=13,
#                                                      class_mode='categorical',
#                                                      batch_size=32,
#                                                      subset="training")
#
# test_datagen = data_generator.flow_from_directory(downloads,
#                                                           target_size=(128, 128),
#                                                           shuffle=True, seed=13,
#                                                           class_mode='categorical',
#                                                           batch_size=10,
#                                                           subset="validation")

train_datagen = ImageDataGenerator(rescale=1. / 255,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True
                                   )

test_datagen = ImageDataGenerator(rescale=1. / 255)

training_set = train_datagen.flow_from_directory('train_data',
                                                 target_size=(256, 256),
                                                 batch_size=32,
                                                 class_mode='categorical',
                                                 shuffle=True
                                                 # color_mode="rgb",
                                                 # seed=16
                                                 )

test_set = test_datagen.flow_from_directory('test_data',
                                            target_size=(256, 256),
                                            batch_size=10,
                                            class_mode='categorical',
                                            shuffle=True
                                            # color_mode="rgb",
                                            # seed=16
                                            )

model.fit_generator(training_set,
                    steps_per_epoch=789,    # 25276 images
                    epochs=50,
                    validation_data=test_set,
                    validation_steps=281   # 2810 images
                    )

model.save('cnn_classifier2.h5')

